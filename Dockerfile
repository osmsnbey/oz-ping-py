FROM python:alpine

VOLUME /src/
COPY ozpyping.py requirements.txt config.ini /src/
ADD oztest /src/oztest
WORKDIR /src

RUN pip install -r requirements.txt

CMD ["python", "-u", "/src/ozpyping.py"]
