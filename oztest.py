import argparse

from oztest.OZSpeedtest import OZSpeedtest

parser = argparse.ArgumentParser(description="A tool to take network speed test and send the results to InfluxDB")
args = parser.parse_args()
collector = OZSpeedtest()
collector.run()
