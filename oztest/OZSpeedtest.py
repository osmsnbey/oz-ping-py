import timeit
start_time = timeit.default_timer()

import os, sys
import subprocess
import time, datetime, pytz
from datetime import datetime

from oztest.common import log 
from oztest.common import config

import csv
import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode


class OZSpeedtest():
    def __init__(self) -> None:
        super().__init__()
        self.db_client = self._get_db_connection()

    def _get_db_connection(self):
        
        conn = mysql.connector.connect(host=config.db_address,
                               port=config.db_port,
                               database=config.database,
                               user=config.user,
                               password=config.password,
                               auth_plugin=config.auth_plugin) # added this line 
    def run_speed_test(self, server=None):
        log.info('Starting Test')
        

    def run (self):
        while True:
            self.run_speed_tst()
            log.info('Running', config.delay)
            time.sleep(config.delay)