
import subprocess
import sys
import os
import csv
from datetime import datetime
import random

pinghosts = ['8.8.8.8', '8.8.4.4', '4.2.2.2' ,'208.67.222.222', '208.67.220.220', '1.1.1.1', 'heise.de', 'ibm.com', 'upc.at', 'linkedin.com', 'xing.com', 'orf.at', 'kurier.at', 'derstandard.at', 'example.com', 'meter.net', 'vultr.net', 'vps.net', 'linode.com', 'aruba.it', 'speedtest.power-speed.at', 'speedtest.citycom-austria.com', 'speedtest.net']

pingconfig = {'host': random.choice(pinghosts), 'timeout': 5, 'min': 0.0, 'avg': 0.0, 'max': 0.0, 'mdev': 0.0 }
host = pingconfig['host']
cmd = ['ping', '-c2', '-W 5', pingconfig['host'] ]
done = False
timeout = pingconfig ['timeout'] # default time out after x times, set to -1 to disable timeout

pingdata = {}
pingdata.update({'timestamp': datetime.now().strftime('%Y.%m.%d %H:%M:%S')})
pingdata.update({'host': pingconfig['host']})
pingdata.update({'min': 0.0})
pingdata.update({'avg': 0.0})
pingdata.update({'max': 0.0})
pingdata.update({'mdev': 0.0})
rtt=[]
while not done and timeout:
        response = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        stdout, stderr = response.communicate()
        if response.returncode == 0:
            s = str(stdout)
            sp = s.split()
            sys.stdout.write (str(sp))
            pingdata.update({'host': sp[1]})
            rtt = sp[-2].split('/')
            sys.stdout.write (str(rtt))
            pingdata.update({'min': rtt[0]})
            pingdata.update({'avg': rtt[1]})
            pingdata.update({'max': rtt[2]})
            pingdata.update({'mdev': rtt[3]})
            done = True
        else:
            sys.stdout.write('.')
            timeout -= 1
if not done:
       sys.stderr.write ("\nServer failed to respond: " + pingconfig['host'])


fields = ['timestamp', 'host', 'min', 'avg', 'max', 'mdev']
filename = 'pingtest.csv'

filenotexists = os.path.exists(filename)
print (filenotexists)

with open (filename, 'a') as csvfile:
    writer = csv.DictWriter (csvfile, fields)
    if not filenotexists:
        writer.writeheader()

    writer.writerow (pingdata)

