import configparser
import os
import sys


class ConfigManager():

    def __init__(self, config):
        print('Loading Configuration File {}'.format(config))
        self.servers = []
        config_file = os.path.join(os.getcwd(), config)
        if os.path.isfile(config_file):
            self.config = configparser.ConfigParser()
            self.config.read(config_file)
        else:
            print('ERROR: Unable To Load Config File: {}'.format(config_file))
            sys.exit(1)

        self._load_config_values()
        print('Configuration Successfully Loaded')

    def _load_config_values(self):

        # General
        self.delay = self.config['GENERAL'].getint('Delay', fallback=300)

        # DBDATA
        self.db_address = self.config['DBDATA']['Address']
        self.db_port = self.config['DBDATA'].getint('Port', fallback=3306)
        self.db_database = self.config['DBDATA'].get('Database', fallback='pingtest')
        self.db_user = self.config['DBDATA'].get('Username', fallback='')
        self.db_password = self.config['DBDATA'].get('Password', fallback='')
        
        # self.db_ssl = self.config['DBDATA'].getboolean('SSL', fallback=False)
        # self.db_verify_ssl = self.config['DBDATA'].getboolean('Verify_SSL', fallback=True)
        self.auth_plugin = self.config['DBDATA'].get('Auth_Plugin', fallback='mysql_native_pass')

        # Logging
        self.logging_level = self.config['LOGGING'].get('Level', fallback='debug')
        self.logging_level = self.logging_level.upper()

        # Speedtest
        test_server = self.config['SPEEDTEST'].get('Server', fallback=None)
        if test_server:
            self.servers = test_server.split(',')
