import os

from oztest.config.configmanager import ConfigManager

if os.getenv('ozspeedtest'):
    config = os.getenv('ozspeedtest')
else:
    config = 'config.ini'

config = ConfigManager(config)